export function pageLoad() {

    window.addEventListener("load", (event) => {
        
        const footer = document.querySelector(".app-footer");
    
        const section = document.createElement("section");
        section.className = "app-section";
    
        const heading = document.createElement("h2");
        heading.className = "app-title";
        heading.innerText = "Join Our Program";
    
        const h3 = document.createElement("h3");
        h3.className = "app-subtitle";
        h3.innerHTML = "Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua."
    
        const button = document.createElement("button");
        button.type = "submit"
        button.className = "app-section__button";
        button.innerText = "SUBSCRIBE";
    
        const form = document.createElement("form");
        form.className = "app-email-form";
        
        const emailInput = document.createElement("input");
        emailInput.type = "email";
        emailInput.placeholder = "email";
        
        footer.before(section);
        section.appendChild(heading);
        section.appendChild(h3);
        section.appendChild(form);

        form.appendChild(emailInput);
        form.appendChild(button);


        form.addEventListener("submit", (e) => {
            e.preventDefault();
            console.log(emailInput.value);
                })
    
      });

      window.onbeforeunload = function() {
        return false; }
}
