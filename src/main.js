import { pageLoad } from "./join-us-section.js";

export const main = pageLoad;

class Standard {
    constructor(test) {
        this.test = test;
    }
}

class Advanced {
    constructor(test) {
        this.test = test;
    }
}

class SectionCreator {
    createSection(type) {
        switch(type) {
            case "standard":
                return new Standard("");
            case "advanced":
                return new Advanced("")
        }
    }
}